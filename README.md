# FindClasses

This is a simple script which is used to see which classes in a jar(first argument of script) are actually being imported and used in the project. This script will 
also copy the sources of the classes(from the third argument which is the location of sources of the jar) to a new directory(fourth argument of the script). 

Usage: findClasses path/to/jar path/to/search path/to/source path/to/copy/sources

eg: ./findClasses ./intellij-core.jar ./kotlin-1.1.1 ./kotlin_friends/intellij-community-163 ./kotlin_friends/intellij-core-needs
<br/>mind hwo there a no trailing / after any of paths